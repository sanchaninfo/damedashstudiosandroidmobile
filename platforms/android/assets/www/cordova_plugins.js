cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
  {
    "id": "com.hutchind.cordova.plugins.streamingmedia.StreamingMedia",
    "file": "plugins/com.hutchind.cordova.plugins.streamingmedia/www/StreamingMedia.js",
    "pluginId": "com.hutchind.cordova.plugins.streamingmedia",
    "clobbers": [
      "streamingMedia"
    ]
  },
  {
    "id": "cordova-plugin-device.device",
    "file": "plugins/cordova-plugin-device/www/device.js",
    "pluginId": "cordova-plugin-device",
    "clobbers": [
      "device"
    ]
  },
  {
    "id": "cordova-plugin-inappbrowser.inappbrowser",
    "file": "plugins/cordova-plugin-inappbrowser/www/inappbrowser.js",
    "pluginId": "cordova-plugin-inappbrowser",
    "clobbers": [
      "cordova.InAppBrowser.open",
      "window.open"
    ]
  },
  {
    "id": "cordova-plugin-speechrecognizer.SpeechRecognizer",
    "file": "plugins/cordova-plugin-speechrecognizer/SpeechRecognizer.js",
    "pluginId": "cordova-plugin-speechrecognizer",
    "clobbers": [
      "plugins.speechrecognizer"
    ]
  },
  {
    "id": "cordova-plugin-appavailability.AppAvailability",
    "file": "plugins/cordova-plugin-appavailability/www/AppAvailability.js",
    "pluginId": "cordova-plugin-appavailability",
    "clobbers": [
      "appAvailability"
    ]
  },
  {
    "id": "cordova-plugin-splashscreen.SplashScreen",
    "file": "plugins/cordova-plugin-splashscreen/www/splashscreen.js",
    "pluginId": "cordova-plugin-splashscreen",
    "clobbers": [
      "navigator.splashscreen"
    ]
  },
  {
    "id": "es6-promise-plugin.Promise",
    "file": "plugins/es6-promise-plugin/www/promise.js",
    "pluginId": "es6-promise-plugin",
    "runs": true
  },
  {
    "id": "cordova-plugin-x-socialsharing.SocialSharing",
    "file": "plugins/cordova-plugin-x-socialsharing/www/SocialSharing.js",
    "pluginId": "cordova-plugin-x-socialsharing",
    "clobbers": [
      "window.plugins.socialsharing"
    ]
  },
  {
    "id": "cordova-sqlite-storage.SQLitePlugin",
    "file": "plugins/cordova-sqlite-storage/www/SQLitePlugin.js",
    "pluginId": "cordova-sqlite-storage",
    "clobbers": [
      "SQLitePlugin"
    ]
  }
];
module.exports.metadata = 
// TOP OF METADATA
{
  "com.hutchind.cordova.plugins.streamingmedia": "0.1.4",
  "cordova-plugin-device": "1.1.4-dev",
  "cordova-plugin-inappbrowser": "1.5.0",
  "cordova-plugin-speechrecognizer": "1.0.0",
  "cordova-plugin-whitelist": "1.3.0",
  "cordova-plugin-appavailability": "0.4.2",
  "cordova-plugin-splashscreen": "4.1.0",
  "es6-promise-plugin": "4.1.0",
  "cordova-plugin-x-socialsharing": "5.2.1",
  "cordova-sqlite-storage": "2.1.4"
};
// BOTTOM OF METADATA
});